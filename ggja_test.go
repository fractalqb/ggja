package ggja

import (
	"fmt"
	"math"
	"testing"
)

func printError(err error) {
	fmt.Printf("ERROR: %s\n", err)
}

type onErr struct {
	t *testing.T
}

func (oe onErr) error(err error) {
	oe.t.Error(err)
}

func (oe onErr) fatal(err error) {
	oe.t.Fatal(err)
}

func ExampleGet() {
	data := BareObj{
		"foo": "bar",
		"baz": BareArr{
			true, false,
			BareObj{"OK": math.Pi},
			"end",
		},
	}
	fmt.Println(Get(data, "baz", -2))
	// Output:
	// map[OK:3.141592653589793] <nil>
}

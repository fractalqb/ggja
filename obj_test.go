package ggja

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"
)

func TestObj_CallNPut1(t *testing.T) {
	var set = func(o *Obj) {
		o.Put("key", "val")
	}
	var obj = Obj{OnError: onErr{t}.error}
	set(&obj)
	if obj.MStr("key") != "val" {
		t.Fatal("failed to set key to val")
	}
}

func TestObj_CallNPut2(t *testing.T) {
	var set = func(o Obj) {
		o.Set("key1", "val1")
	}
	var obj = Obj{OnError: onErr{t}.error}
	obj.Put("key1", "-")
	obj.Put("key2", "-")
	set(obj)
	if obj.MStr("key1") != "val1" {
		t.Fatal("failed to set key to val")
	}
	obj.Set("key2", "val2")
	if obj.MStr("key2") != "val2" {
		t.Fatal("failed to set key to val")
	}
}

func ExampleObj_Put() {
	jBar := Obj{OnError: printError}
	jBar.Put("foo", 4711)
	jBar.CObj("baz").Put("quux", true)
	err := json.NewEncoder(os.Stdout).Encode(jBar.Bare)
	if err != nil {
		fmt.Println(err)
	}
	// Output:
	// {"baz":{"quux":true},"foo":4711}
}

func ExampleObj_Sets() {
	bar := map[string]interface{}{"foo": 4711}
	jBar := Obj{Bare: bar, OnError: printError}
	jBar.Set("foo", "baz")
	fmt.Println(jBar.Bare)
	jBar.Set("baz", "should fail")
	fmt.Println(jBar.MBool("baz"))
	// Output:
	// map[foo:baz]
	// ERROR: Object member 'baz': Cannot be set, member does not exist
	// ERROR: Object member 'baz': Member does not exist
	// false
}

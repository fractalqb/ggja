package ggja

import (
	"encoding/json"
	"fmt"
)

func ExampleArr_CObj() {
	ggjaArr := Arr{OnError: printError}
	objMbr3 := ggjaArr.CObj(3)
	objMbr3.Put("name", "John Doe")
	jStr, _ := json.Marshal(ggjaArr.Bare)
	fmt.Println(string(jStr))
	jStr, _ = json.Marshal(objMbr3.Bare)
	fmt.Println(string(jStr))
	// Output:
	// [null,null,null,{"name":"John Doe"}]
	// {"name":"John Doe"}
}

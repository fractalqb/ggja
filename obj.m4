changequote(`<[', `]>')dnl
define(<[primitive]>, <[func (o *Obj) $1(key string, nvl $2) $2 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := as$1(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	}
	return nvl
}

func (o *Obj) M$1(key string) $2 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := as$1(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	} else {
		o.fail(notExists(key))
	}
	return $3
}
]>)dnl
package ggja

primitive(Uint8,uint8,0)
primitive(Uint16,uint16,0)
primitive(Uint32,uint32,0)
primitive(Uint64,uint64,0)
primitive(Int8,int8,0)
primitive(Int16,int16,0)
primitive(Int32,int32,0)
primitive(Int64,int64,0)
primitive(F32,float32,0)
primitive(F64,float64,0)
primitive(C64,complex64,0)
primitive(C128,complex128,0)
primitive(Byte,byte,0)
primitive(Rune,rune,0)
primitive(Uint,uint,0)
primitive(Int,int,0)
primitive(UintPtr,uintptr,0)

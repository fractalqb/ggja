package ggja

import (
	"errors"
	"fmt"
)

type BareArr = []interface{}

type IndexError struct {
	Idx, EIdx int
	err       error
}

func (ee IndexError) Error() string {
	return fmt.Sprintf("Array index %d [%d]: %s", ee.Idx, ee.EIdx, ee.err)
}

func (ee IndexError) Unwrap() error { return ee.err }

type IdxRangeError struct {
	Begin, End int
}

func (eir IdxRangeError) Error() string {
	return fmt.Sprintf("Out of [%d; %d) range", eir.Begin, eir.End)
}

type Arr struct {
	Bare BareArr
	// When OnError is nil all methods will panic on error, otherwise
	// OnError(err) will be called. 'M*' methods returing values will return
	// zero value on error if OnError has no non-local exists. Non-'M*' methods
	// retrun the nvl value.
	OnError func(error)
}

func (a *Arr) Len() int {
	if a == nil {
		return 0
	}
	return len(a.Bare)
}

func (a *Arr) Ignoring(errs ...error) *Arr {
	return &Arr{
		Bare: a.Bare,
		OnError: IgnoreErrors{
			Ignore: errs,
			Else:   a.OnError,
		}.Error,
	}
}

func (a *Arr) Set(idx int, v interface{}) *Arr {
	eidx := a.adjIdx(idx)
	if eidx < 0 || eidx > len(a.Bare) {
		a.fail(IndexError{idx, eidx, IdxRangeError{0, len(a.Bare)}})
	} else {
		a.Bare[eidx] = v
	}
	return a
}

func (a *Arr) Put(idx int, v interface{}) *Arr {
	eidx := a.adjIdx(idx)
	if eidx < 0 {
		a.fail(IndexError{idx, eidx, errors.New("Negative effective index")})
	}
	if eidx >= len(a.Bare) {
		if eidx >= cap(a.Bare) {
			nb := make([]interface{}, eidx+1)
			copy(nb, a.Bare)
			a.Bare = nb
		} else {
			a.Bare = a.Bare[:eidx+1]
		}
	}
	a.Bare[eidx] = v
	return a
}

func (a *Arr) Obj(idx int) *Obj {
	eidx := a.adjIdx(idx)
	if a == nil || eidx < 0 || eidx >= len(a.Bare) {
		return nil
	}
	if tmp := a.Bare[eidx]; tmp == nil {
		return nil
	} else if obj, ok := tmp.(BareObj); ok {
		return &Obj{Bare: obj, OnError: a.OnError}
	} else {
		a.fail(IndexError{idx, eidx, fmt.Errorf("Array element is no object: '%v'", tmp)})
		return nil
	}
}

func (a *Arr) CObj(idx int) (elm *Obj) {
	if elm = a.Obj(idx); elm == nil {
		obj := make(BareObj)
		a.Put(idx, obj)
		elm = &Obj{Bare: obj, OnError: a.OnError}
	}
	return elm
}

func (a *Arr) MObj(idx int) *Obj {
	eidx := a.adjIdx(idx)
	if eidx < 0 || eidx >= len(a.Bare) {
		a.fail(IndexError{idx, eidx, IdxRangeError{0, len(a.Bare)}})
		return nil
	}
	if tmp := a.Bare[eidx]; tmp == nil {
		a.fail(fmt.Errorf("Not an object element in array at %d", eidx))
		return nil
	} else if obj, ok := tmp.(BareObj); ok {
		return &Obj{Bare: obj, OnError: a.OnError}
	} else {
		a.fail(IndexError{idx, eidx, fmt.Errorf("not an object: '%v'", tmp)})
		return nil
	}
}

func (a *Arr) Arr(idx int) *Arr {
	eidx := a.adjIdx(idx)
	if a == nil || eidx < 0 || eidx >= len(a.Bare) {
		return nil
	}
	if tmp := a.Bare[eidx]; tmp == nil {
		return nil
	} else if arr, ok := tmp.(BareArr); ok {
		return &Arr{Bare: arr, OnError: a.OnError}
	} else {
		a.fail(IndexError{idx, eidx, fmt.Errorf("Element is not array: '%v'", tmp)})
		return nil
	}
}

func (a *Arr) CArr(idx int) (elm *Arr) {
	if elm = a.Arr(idx); elm == nil {
		arr := make(BareArr, 0)
		a.Put(idx, arr)
		elm = &Arr{Bare: arr, OnError: a.OnError}
	}
	return elm
}

func (a *Arr) MArr(idx int) *Arr {
	eidx := a.adjIdx(idx)
	if eidx < 0 || eidx >= len(a.Bare) {
		a.fail(IndexError{idx, eidx, IdxRangeError{0, len(a.Bare)}})
		return nil
	}
	if tmp := a.Bare[eidx]; tmp == nil {
		a.fail(IndexError{idx, eidx, fmt.Errorf("Not an array element")})
		return nil
	} else if arr, ok := tmp.(BareArr); ok {
		return &Arr{Bare: arr, OnError: a.OnError}
	} else {
		a.fail(IndexError{idx, eidx, fmt.Errorf("Array element is no array: '%v'", tmp)})
		return nil
	}
}

func (a *Arr) Bool(idx int, nvl bool) bool {
	eidx := a.adjIdx(idx)
	if a == nil || eidx < 0 || eidx >= len(a.Bare) {
		return nvl
	}
	if tmp := a.Bare[eidx]; tmp == nil {
		return nvl
	} else if res, ok := tmp.(bool); ok {
		return res
	} else {
		a.fail(IndexError{
			idx, eidx,
			fmt.Errorf("Array element is not boolean: '%v'", tmp),
		})
		return nvl
	}
}

func (a *Arr) MBool(idx int) bool {
	eidx := a.adjIdx(idx)
	if eidx < 0 || eidx >= len(a.Bare) {
		a.fail(IndexError{idx, eidx, IdxRangeError{0, len(a.Bare)}})
		return false
	}
	if tmp := a.Bare[eidx]; tmp == nil {
		a.fail(IndexError{idx, eidx, errors.New("No boolean array element")})
		return false
	} else if res, ok := tmp.(bool); ok {
		return res
	} else {
		a.fail(IndexError{
			idx, eidx,
			fmt.Errorf("Array element is not boolean: '%v'", tmp),
		})
		return false
	}
}

func (a *Arr) F64(idx int, nvl float64) float64 {
	eidx := a.adjIdx(idx)
	if eidx < 0 || eidx >= len(a.Bare) {
		return nvl
	}
	if tmp := a.Bare[eidx]; tmp == nil {
		return nvl
	} else if res, ok := tmp.(float64); ok {
		return res
	} else {
		a.fail(IndexError{
			idx, eidx,
			fmt.Errorf("Array element is no float64: '%v'", tmp)})
		return 0
	}
}

func (a *Arr) MF64(idx int) float64 {
	eidx := a.adjIdx(idx)
	if eidx < 0 || eidx >= len(a.Bare) {
		a.fail(IndexError{idx, eidx, IdxRangeError{0, len(a.Bare)}})
		return 0
	}
	tmp := a.Bare[eidx]
	if tmp == nil {
		a.fail(IndexError{idx, eidx, errors.New("No float64 array element")})
		return 0
	} else if res, ok := tmp.(float64); ok {
		return res
	}
	a.fail(IndexError{
		idx, eidx,
		fmt.Errorf("Array element is not float64: '%v'", tmp),
	})
	return 0
}

func (a *Arr) F32(idx int, nvl float32) float32 {
	eidx := a.adjIdx(idx)
	if eidx < 0 || eidx >= len(a.Bare) {
		return nvl
	}
	tmp := a.Bare[eidx]
	if tmp == nil {
		return nvl
	} else if res, ok := tmp.(float64); ok {
		return float32(res)
	}
	a.fail(IndexError{
		idx, eidx,
		fmt.Errorf("Array element is no float32: '%v'", tmp)})
	return 0
}

// TODO missleading error messages
func (a *Arr) MF32(idx int) float32 { return float32(a.MF64(idx)) }

func (a *Arr) Int(idx int, nvl int) int {
	eidx := a.adjIdx(idx)
	if a == nil || eidx < 0 || eidx >= len(a.Bare) {
		return nvl
	}
	if tmp := a.Bare[eidx]; tmp == nil {
		return nvl
	} else if res, err := asInt(tmp); err != nil {
		a.fail(IndexError{idx, eidx, err})
		return nvl
	} else {
		return res
	}
}

func (a *Arr) MInt(idx int) int {
	eidx := a.adjIdx(idx)
	if eidx < 0 || eidx >= len(a.Bare) {
		a.fail(IndexError{idx, eidx, IdxRangeError{0, len(a.Bare)}})
		return 0
	}
	if tmp := a.Bare[eidx]; tmp == nil {
		a.fail(IndexError{idx, eidx, errors.New("No int array element")})
		return 0
	} else if res, err := asInt(tmp); err != nil {
		a.fail(IndexError{idx, eidx, err})
		return 0
	} else {
		return res
	}
}

func (a *Arr) Uint32(idx int, nvl uint32) uint32 {
	eidx := a.adjIdx(idx)
	if a == nil || eidx < 0 || eidx >= len(a.Bare) {
		return nvl
	}
	if tmp := a.Bare[eidx]; tmp == nil {
		return nvl
	} else if res, err := asUint32(tmp); err != nil {
		a.fail(IndexError{idx, eidx, err})
		return nvl
	} else {
		return res
	}
}

func (a *Arr) MUint32(idx int) uint32 {
	eidx := a.adjIdx(idx)
	if eidx < 0 || eidx >= len(a.Bare) {
		a.fail(IndexError{idx, eidx, IdxRangeError{0, len(a.Bare)}})
		return 0
	}
	if tmp := a.Bare[eidx]; tmp == nil {
		a.fail(IndexError{idx, eidx, errors.New("No uint32 array element at")})
		return 0
	} else if res, err := asUint32(tmp); err != nil {
		a.fail(IndexError{idx, eidx, err})
		return 0
	} else {
		return res
	}
}

func (a *Arr) Str(idx int, nvl string) string {
	eidx := a.adjIdx(idx)
	if a == nil || eidx < 0 || eidx >= len(a.Bare) {
		return ""
	}
	if tmp := a.Bare[eidx]; tmp == nil {
		return ""
	} else if res, ok := tmp.(string); ok {
		return res
	} else {
		a.fail(IndexError{idx, eidx, fmt.Errorf("Array element is no string: '%v'", tmp)})
		return ""
	}
}

func (a *Arr) MStr(idx int) string {
	eidx := a.adjIdx(idx)
	if eidx < 0 || eidx >= len(a.Bare) {
		a.fail(IndexError{idx, eidx, IdxRangeError{0, len(a.Bare)}})
		return ""
	}
	if tmp := a.Bare[eidx]; tmp == nil {
		a.fail(IndexError{idx, eidx, errors.New("No string array element")})
		return ""
	} else if res, ok := tmp.(string); ok {
		return res
	} else {
		a.fail(IndexError{idx, eidx, fmt.Errorf("Array element is no string: '%v'", tmp)})
		return ""
	}
}

func (a *Arr) adjIdx(idx int) int {
	if idx < 0 {
		return len(a.Bare) + idx
	}
	return idx
}

func (a *Arr) fail(err error) {
	if a.OnError == nil {
		panic(err)
	} else {
		a.OnError(err)
	}
}

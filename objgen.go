package ggja

func (o *Obj) Uint8(key string, nvl uint8) uint8 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asUint8(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	}
	return nvl
}

func (o *Obj) MUint8(key string) uint8 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asUint8(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	} else {
		o.fail(notExists(key))
	}
	return 0
}

func (o *Obj) Uint16(key string, nvl uint16) uint16 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asUint16(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	}
	return nvl
}

func (o *Obj) MUint16(key string) uint16 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asUint16(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	} else {
		o.fail(notExists(key))
	}
	return 0
}

func (o *Obj) Uint32(key string, nvl uint32) uint32 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asUint32(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	}
	return nvl
}

func (o *Obj) MUint32(key string) uint32 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asUint32(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	} else {
		o.fail(notExists(key))
	}
	return 0
}

func (o *Obj) Uint64(key string, nvl uint64) uint64 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asUint64(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	}
	return nvl
}

func (o *Obj) MUint64(key string) uint64 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asUint64(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	} else {
		o.fail(notExists(key))
	}
	return 0
}

func (o *Obj) Int8(key string, nvl int8) int8 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asInt8(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	}
	return nvl
}

func (o *Obj) MInt8(key string) int8 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asInt8(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	} else {
		o.fail(notExists(key))
	}
	return 0
}

func (o *Obj) Int16(key string, nvl int16) int16 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asInt16(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	}
	return nvl
}

func (o *Obj) MInt16(key string) int16 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asInt16(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	} else {
		o.fail(notExists(key))
	}
	return 0
}

func (o *Obj) Int32(key string, nvl int32) int32 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asInt32(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	}
	return nvl
}

func (o *Obj) MInt32(key string) int32 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asInt32(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	} else {
		o.fail(notExists(key))
	}
	return 0
}

func (o *Obj) Int64(key string, nvl int64) int64 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asInt64(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	}
	return nvl
}

func (o *Obj) MInt64(key string) int64 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asInt64(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	} else {
		o.fail(notExists(key))
	}
	return 0
}

func (o *Obj) F32(key string, nvl float32) float32 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asF32(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	}
	return nvl
}

func (o *Obj) MF32(key string) float32 {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asF32(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	} else {
		o.fail(notExists(key))
	}
	return 0
}

func (o *Obj) Int(key string, nvl int) int {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asInt(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	}
	return nvl
}

func (o *Obj) MInt(key string) int {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asInt(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	} else {
		o.fail(notExists(key))
	}
	return 0
}

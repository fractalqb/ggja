package ggja

import (
	"fmt"
	"math"
	"time"
)

/*
uint8  : 0 to 255
uint16 : 0 to 65535
uint32 : 0 to 4294967295
uint64 : 0 to 18446744073709551615
int8   : -128 to 127
int16  : -32768 to 32767
int64  : -9223372036854775808 to 9223372036854775807
*/

type NoConversionError struct {
	Value  interface{}
	Target string
}

func (nce NoConversionError) Error() string {
	return fmt.Sprintf("No conversion to %s from %T", nce.Target, nce.Value)
}

const maxUint = ^uint(0)
const maxInt = int(maxUint >> 1)
const minInt = -maxInt - 1

func intRangef(x float64) bool {
	return minInt <= int(x) && int(x) <= maxInt
}

func int8Rangei(x int64) bool {
	return math.MinInt8 <= x && x <= math.MaxInt8
}

func int8Rangef(x float64) bool {
	return math.MinInt8 <= x && x <= math.MaxInt8
}

func uint8Rangei(x int64) bool {
	return 0 <= x && x <= math.MaxUint8
}

func uint8Rangef(x float64) bool {
	return 0 <= x && x <= math.MaxUint8
}

func int16Rangef(x float64) bool {
	return math.MinInt16 <= x && x <= math.MaxInt16
}

func uint16Rangei(x int64) bool {
	return 0 <= x && x <= math.MaxUint16
}

func uint16Rangef(x float64) bool {
	return 0 <= x && x <= math.MaxUint16
}

func uint32Rangei(x int64) bool {
	return 0 <= x && x <= math.MaxUint32
}

func uint32Rangef(x float64) bool {
	return 0 <= x && x <= math.MaxUint32
}

func int32Rangef(x float64) bool {
	return math.MinInt32 <= x && x <= math.MaxInt32
}

func int32Rangei(x int64) bool {
	return math.MinInt32 <= x && x <= math.MaxInt32
}

func uint64Rangef(x float64) bool {
	return 0 <= x && x <= math.MaxUint64
}

func int64Rangef(x float64) bool {
	return math.MinInt64 <= x && x <= math.MaxInt64
}

func f32Range(x float64) bool {
	return math.Abs(x) <= math.MaxFloat32
}

func asInt(v interface{}) (int, error) {
	switch res := v.(type) {
	case int:
		return res, nil
	case float64:
		if intRangef(res) {
			return int(res), nil
		}
		return 0, fmt.Errorf("float64 value out of int range: %f", res)
	}
	return 0, NoConversionError{v, "int"}
}

func asInt8(v interface{}) (int8, error) {
	switch res := v.(type) {
	case float64:
		if int8Rangef(res) {
			return int8(res), nil
		}
		return 0, fmt.Errorf("float64 value out of int8 range: %f", res)
	}
	return 0, NoConversionError{v, "int8"}
}

func asInt16(v interface{}) (int16, error) {
	switch res := v.(type) {
	case float64:
		if int16Rangef(res) {
			return int16(res), nil
		}
		return 0, fmt.Errorf("float64 value out of int16 range: %f", res)
	}
	return 0, NoConversionError{v, "int16"}
}

func asInt32(v interface{}) (int32, error) {
	switch res := v.(type) {
	case int64:
		if int32Rangei(res) {
			return int32(res), nil
		}
		return 0, fmt.Errorf("float64 value out of int32 range: %d", res)
	case int32:
		return res, nil
	case float64:
		if int32Rangef(res) {
			return int32(res), nil
		}
		return 0, fmt.Errorf("float64 value out of int32 range: %f", res)
	}
	return 0, NoConversionError{v, "int32"}
}

func asUint8(v interface{}) (uint8, error) {
	switch res := v.(type) {
	case int64:
		if uint8Rangei(res) {
			return uint8(res), nil
		}
		return 0, fmt.Errorf("int64 value out of uint8 range: %d", res)
	case float64:
		if uint8Rangef(res) {
			return uint8(res), nil
		}
		return 0, fmt.Errorf("float64 value out of uint8 range: %f", res)
	}
	return 0, NoConversionError{v, "uint8"}
}

func asUint16(v interface{}) (uint16, error) {
	switch res := v.(type) {
	case int64:
		if uint16Rangei(res) {
			return uint16(res), nil
		}
		return 0, fmt.Errorf("int64 value out of uint16 range: %d", res)
	case float64:
		if uint16Rangef(res) {
			return uint16(res), nil
		}
		return 0, fmt.Errorf("float64 value out of uint16 range: %f", res)
	}
	return 0, NoConversionError{v, "uint16"}
}

func asUint32(v interface{}) (uint32, error) {
	switch res := v.(type) {
	case int64:
		if uint32Rangei(res) {
			return uint32(res), nil
		}
		return 0, fmt.Errorf("int64 value out of uint32 range: %d", res)
	case float64:
		if uint32Rangef(res) {
			return uint32(res), nil
		}
		return 0, fmt.Errorf("float64 value out of uint64 range: %f", res)
	}
	return 0, NoConversionError{v, "uint32"}
}

func asInt64(v interface{}) (int64, error) {
	switch res := v.(type) {
	case int64:
		return res, nil
	case float64:
		if int64Rangef(res) {
			return int64(res), nil
		} else {
			return 0, fmt.Errorf("float64 value out of int64 range: %f", res)
		}
	}
	return 0, NoConversionError{v, "int64"}
}

func asUint64(v interface{}) (uint64, error) {
	switch res := v.(type) {
	case uint64:
		return res, nil
	case int64:
		if res < 0 {
			return 0, fmt.Errorf("Cannot convert negative int64 %d to uint64", res)
		}
		return uint64(res), nil
	case float64:
		if uint64Rangef(res) {
			return uint64(res), nil
		}
		return 0, fmt.Errorf("float64 value out of uint64 range: %f", res)
	}
	return 0, NoConversionError{v, "uint64"}
}

func asF32(v interface{}) (float32, error) {
	switch res := v.(type) {
	case float32:
		return res, nil
	case float64:
		if f32Range(res) {
			return float32(res), nil
		}
		return 0, fmt.Errorf("float64 value out of float32 range: %f", res)
	}
	return 0, NoConversionError{v, "float32"}
}

func asTime(v interface{}) (time.Time, error) {
	switch res := v.(type) {
	case string:
		return time.Parse(time.RFC3339, res)
	}
	return time.Time{}, NoConversionError{v, "time.Time"}
}

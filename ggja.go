package ggja

import (
	"fmt"
)

func GetString(v interface{}, path ...interface{}) (string, error) {
	tmp, err := Get(v, path...)
	if err != nil {
		return "", err
	}
	if s, ok := tmp.(string); ok {
		return s, nil
	}
	return "", fmt.Errorf("Path %s resolves to %T not string", path, tmp)
}

func GetObj(v interface{}, path ...interface{}) (*Obj, error) {
	tmp, err := Get(v, path...)
	if err != nil {
		return nil, err
	}
	if bo, ok := tmp.(BareObj); ok {
		return &Obj{Bare: bo, OnError: errHdlr(v)}, nil
	}
	return nil, fmt.Errorf("Path %s resolves to %T not BareObj", path, tmp)
}

func errHdlr(v interface{}) func(error) {
	switch tmp := v.(type) {
	case Obj:
		return tmp.OnError
	case Arr:
		return tmp.OnError
	}
	return nil
}

func Get(v interface{}, path ...interface{}) (interface{}, error) {
	for i := 0; i < len(path); i++ {
		switch p := path[i].(type) {
		case string:
			var bobj BareObj
			switch o := v.(type) {
			case *Obj:
				bobj = o.Bare
			case Obj:
				bobj = o.Bare
			case BareObj:
				bobj = o
			default:
				return nil, fmt.Errorf("Cannot access %T element %d by string", o, i)
			}
			if e, ok := bobj[p]; ok {
				v = e
			} else {
				return nil, fmt.Errorf("Cannot resolv path element %d '%s'", i, p)
			}
		case int:
			var barr BareArr
			switch a := v.(type) {
			case *Arr:
				barr = a.Bare
			case Arr:
				barr = a.Bare
			case BareArr:
				barr = a
			default:
				return nil, fmt.Errorf("Cannot access %T element %d by int", a, i)
			}
			ei := p
			if p < 0 {
				ei = len(barr) + ei
			}
			if ei < 0 || ei >= len(barr) {
				return nil, fmt.Errorf("Index %d [%d] out of range [0; %d)",
					p, ei, len(barr))
			}
			v = barr[ei]
		default:
			return nil, fmt.Errorf("Path element %d has invalid type %T", i, p)
		}
	}
	return v, nil
}

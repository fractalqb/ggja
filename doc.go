// Package ggja—the Go Generic JSON Api—is a small helper library to make
// working with Go's generic JSON types more comfortable.
package ggja

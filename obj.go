package ggja

import (
	"errors"
	"fmt"
	"time"
)

type BareObj = map[string]interface{}

type KeyError struct {
	Key string
	err error
}

func (me KeyError) Error() string {
	return fmt.Sprintf("Object member '%s': %s", me.Key, me.err)
}

func (me KeyError) Unwrap() error { return me.err }

func notExists(key string) KeyError {
	return KeyError{key, errors.New("Member does not exist")}
}

type Obj struct {
	Bare BareObj
	// When OnError is nil all methods will panic on error, otherwise
	// OnError(err) will be called. 'M*' methods returing values will return
	// zero value on error if OnError has no non-local exists. Non-'M*' methods
	// retrun the nvl value.
	OnError func(error)
}

func (o *Obj) Ignoring(errs ...error) *Obj {
	return &Obj{
		Bare: o.Bare,
		OnError: IgnoreErrors{
			Ignore: errs,
			Else:   o.OnError,
		}.Error,
	}
}

func (o *Obj) Set(key string, v interface{}) *Obj {
	if o.Bare == nil {
		o.fail(KeyError{key, errors.New("Cannot be set, member does not exist")})
	}
	if _, ok := o.Bare[key]; ok {
		o.Bare[key] = v
	} else {
		o.fail(KeyError{key, errors.New("Cannot be set, member does not exist")})
	}
	return o
}

func (o *Obj) Put(key string, v interface{}) *Obj {
	if o.Bare == nil {
		o.Bare = make(BareObj)
	}
	o.Bare[key] = v
	return o
}

func (o *Obj) Obj(key string) (sub *Obj) {
	if o == nil || o.Bare == nil {
		return nil
	}
	if tmp, ok := o.Bare[key]; ok {
		if res, ok := tmp.(BareObj); ok {
			return &Obj{Bare: res, OnError: o.OnError}
		} else {
			o.fail(KeyError{key, TypeMismatch{"object", tmp}})
		}
	}
	return nil
}

func (o *Obj) CObj(key string) (sub *Obj) {
	if sub = o.Obj(key); sub == nil {
		obj := make(BareObj)
		o.Put(key, obj)
		sub = &Obj{Bare: obj, OnError: o.OnError}
	}
	return sub
}

func (o *Obj) MObj(key string) (sub *Obj) {
	if tmp, ok := o.Bare[key]; ok {
		if res, ok := tmp.(BareObj); ok {
			return &Obj{Bare: res, OnError: o.OnError}
		} else {
			o.fail(KeyError{key, TypeMismatch{"object", tmp}})
		}
	} else {
		o.fail(notExists(key))
	}
	return nil
}

func (o *Obj) Arr(key string) *Arr {
	if o == nil || o.Bare == nil {
		return nil
	}
	if tmp, ok := o.Bare[key]; ok {
		if res, ok := tmp.(BareArr); ok {
			return &Arr{Bare: res, OnError: o.OnError}
		} else {
			o.fail(KeyError{key, TypeMismatch{"array", tmp}})
		}
	}
	return nil
}

func (o *Obj) CArr(key string) (sub *Arr) {
	if sub = o.Arr(key); sub == nil {
		arr := make([]interface{}, 0)
		o.Put(key, arr)
		sub = &Arr{Bare: arr, OnError: o.OnError}
	}
	return sub
}

func (o *Obj) MArr(key string) *Arr {
	if tmp, ok := o.Bare[key]; ok {
		if res, ok := tmp.(BareArr); ok {
			return &Arr{Bare: res, OnError: o.OnError}
		} else {
			o.fail(KeyError{key, TypeMismatch{"array", tmp}})
		}
	} else {
		o.fail(notExists(key))
	}
	return nil
}

func (o *Obj) Bool(key string, nvl bool) bool {
	if tmp, ok := o.Bare[key]; ok {
		if res, ok := tmp.(bool); ok {
			return res
		} else {
			o.fail(KeyError{key, TypeMismatch{"bool", tmp}})
		}
	}
	return nvl
}

func (o *Obj) MBool(key string) bool {
	if tmp, ok := o.Bare[key]; ok {
		if res, ok := tmp.(bool); ok {
			return res
		} else {
			o.fail(KeyError{key, TypeMismatch{"bool", tmp}})
		}
	} else {
		o.fail(notExists(key))
	}
	return false
}

func (o *Obj) F64(key string, nvl float64) float64 {
	if tmp, ok := o.Bare[key]; ok {
		if res, ok := tmp.(float64); ok {
			return res
		} else {
			o.fail(KeyError{key, TypeMismatch{"float64", tmp}})
		}
	}
	return nvl
}

func (o *Obj) MF64(key string) float64 {
	if tmp, ok := o.Bare[key]; ok {
		if res, ok := tmp.(float64); ok {
			return res
		} else {
			o.fail(KeyError{key, TypeMismatch{"float64", tmp}})
		}
	} else {
		o.fail(notExists(key))
	}
	return 0
}

func (o *Obj) Str(key, nvl string) string {
	if tmp, ok := o.Bare[key]; ok {
		if res, ok := tmp.(string); ok {
			return res
		} else {
			o.fail(KeyError{key, TypeMismatch{"string", tmp}})
		}
	}
	return nvl
}

func (o *Obj) MStr(key string) string {
	if tmp, ok := o.Bare[key]; ok {
		if res, ok := tmp.(string); ok {
			return res
		} else {
			o.fail(KeyError{key, TypeMismatch{"string", tmp}})
		}
	} else {
		o.fail(notExists(key))
	}
	return ""
}

func (o *Obj) Time(key string, nvl time.Time) time.Time {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asTime(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	}
	return nvl
}

func (o *Obj) MTime(key string) time.Time {
	if tmp, ok := o.Bare[key]; ok {
		if res, err := asTime(tmp); err != nil {
			o.fail(KeyError{key, err})
		} else {
			return res
		}
	} else {
		o.fail(notExists(key))
	}
	return time.Time{}
}

func (o *Obj) fail(err error) {
	if o.OnError == nil {
		panic(err)
	} else {
		o.OnError(err)
	}
}

package ggja

import (
	"errors"
	"fmt"
)

func Recover(err *error) {
	if p := recover(); p != nil {
		switch x := p.(type) {
		case error:
			*err = x
		case string:
			*err = errors.New(x)
		default:
			*err = fmt.Errorf("panic: %+v", p)
		}
	}
}

type SetError struct {
	Target *error
}

func (es SetError) OnError(err error) { *es.Target = err }

type TypeMismatch struct {
	Expect string
	Value  interface{}
}

func (tmm TypeMismatch) Error() string {
	return fmt.Sprintf("Value is not %s: '%v'", tmm.Expect, tmm.Value)
}

type IgnoreErrors struct {
	Ignore []error
	Else   func(error)
}

func (ign IgnoreErrors) Error(err error) {
	if len(ign.Ignore) == 0 {
		return
	}
	for _, e := range ign.Ignore {
		if errors.Is(err, e) {
			return
		}
	}
	if ign.Else == nil {
		panic(err)
	}
	ign.Else(err)
}
